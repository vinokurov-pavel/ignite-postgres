import javax.cache.configuration.Factory;
import org.apache.ignite.resources.SpringResource;
import org.springframework.jdbc.core.JdbcTemplate;

public class PostgresDBStoreFactory implements Factory<PostgresDBStore> {

    @SpringResource(resourceName = "template")
    transient JdbcTemplate template;

    public void setTemplate(JdbcTemplate template) {
        this.template = template;
    }

    @Override public PostgresDBStore create() {
        return new PostgresDBStore();
    }
}
