import java.util.Collection;
import java.util.stream.Collectors;
import javax.cache.Cache;
import javax.cache.integration.CacheLoaderException;
import javax.cache.integration.CacheWriterException;
import org.apache.ignite.cache.store.CacheStoreAdapter;
import org.apache.ignite.resources.SpringResource;
import org.springframework.jdbc.core.JdbcTemplate;

public class PostgresDBStore extends CacheStoreAdapter<String, String> {


    @SpringResource(resourceName = "template")
    transient JdbcTemplate template;

    @Override public void write(Cache.Entry<? extends String, ? extends String> entry) throws CacheWriterException {

    }

    @Override public void writeAll(
        Collection<Cache.Entry<? extends String, ? extends String>> entries) throws CacheWriterException {
        template.batchUpdate("INSERT INTO test_writeall(val) VALUES(?)",entries.stream().map(entry -> new Object[]{entry.getValue()}).collect(Collectors.toList()));
    }

    @Override public void delete(Object key) throws CacheWriterException {
    }

    @Override public void deleteAll(Collection<?> keys) throws CacheWriterException {
    }

    @Override public String load(String key) throws CacheLoaderException {
        //todo
        return null;
    }
}
