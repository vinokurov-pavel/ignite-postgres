import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import org.apache.ignite.Ignite;
import org.apache.ignite.IgniteCache;
import org.apache.ignite.Ignition;

public class Main {

    public static void main(String[] args) throws InterruptedException {
        try(Ignite ignite = Ignition.start("config/example-cache.xml")){
            IgniteCache<String, String> igniteCache = ignite.cache("testCache");
            igniteCache.clear();
            while(true)
            {

                long start = System.currentTimeMillis();
                Map<String,String> parameterMap = new HashMap<>();
                for(int i = 0; i < 1000; i++)
                {
                    String uuid = UUID.randomUUID().toString();
                    parameterMap.put(uuid, uuid);//use string as key and value
                }
                igniteCache.putAll(parameterMap);
                long end = System.currentTimeMillis();
                System.out.println((end-start)+"ms");
            }
        }
    }
}
